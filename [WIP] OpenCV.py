import cv2
import numpy as np
from PIL import Image

#-----Lecture de l'image en échelle de gris-----
img = cv2.imread('long.png', cv2.IMREAD_GRAYSCALE)
imgC = cv2.imread('long.png', cv2.IMREAD_COLOR)
#-----Inversion de chaque bit de l'array-----
img = cv2.bitwise_not(img)

#-----Variable de taille et on crée une image noire ayant la taille de l'image originale-----
size = np.size(img)
skel = np.zeros(img.shape, np.uint8)

#-----Création de l'image -----
base = np.zeros((28, 28), np.uint8)

#-----Application du seuil-----
ret, img = cv2.threshold(img, 100, 255, cv2.THRESH_BINARY)

#-----Détermination du contour-----
image, contours, hierarchy = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

#-----Obtention des éléments structurants dans une matrice 3x3-----
element = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))

done = False
while (not done):

    zeros = size - cv2.countNonZero(img)
    if zeros == size:
        done = True

    eroded = cv2.erode(img, element)
    temp = cv2.dilate(eroded, element)
    temp = cv2.subtract(img, temp)
    skel = cv2.bitwise_or(skel, temp, skel)
    img = eroded.copy()

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.imshow("FINAL", skel)
cv2.waitKey(0)

imgC = cv2.bitwise_not(imgC)
i = 0

for cnt in contours:
    i += 1
    x,y,w,h = cv2.boundingRect(cnt)
    print("x: " + str(x) + " y: " + str(y) + " W " + str(w) + " H:" + str(h))
    crop_img = imgC[y:y+h, x:x+w]

    #-----Traitement de la taille de l'image-----
    h2, w2 = base.shape
    w_result = w/w2
    print("W result : w:"+str(w)+", w2: "+str(w2) + ", result: " + str(w_result))
    print("W calc : " + str(w/w_result) + ", "+ str(h/w_result))

    h_result = h/h2
    print("H result : h:" + str(h) + ", h2: " + str(h2) + ", result: " + str(h_result))
    print("H calc : " + str(w/h_result) + ", "+ str(h/h_result))

    if(w_result > h_result):
        #small = cv2.resize(crop_img, (0, 0), fx=w_result, fy=w_result)
        small = cv2.resize(crop_img, (int(w/w_result), int(h/w_result)))
        print(str(small.shape))
    else:
        #small = cv2.resize(crop_img, (0, 0), fx=h_result, fy=h_result)
        small = cv2.resize(crop_img, (int(w / h_result), int(h / h_result)))
        print(str(small.shape))

    small = cv2.cvtColor(small, cv2.COLOR_BGR2GRAY)
    x, y = small.shape
    posX = int((base.shape[1] - x) / 2)
    print("PosX: " + str(posX) + ", x: " + str(x) + "sum: " + str(x+posX))
    posY = int((base.shape[0] - y) / 2)
    print("PosY: " + str(posY) + ", y: " + str(y) + "sum: " + str(y+posY))

    base[posX:posX+x, posY:posY+y] = small

    imageName = "character/image"+str(i)+".png"
    cv2.imwrite(imageName, base)

    base = np.zeros((28, 28), np.uint8)

'''
#==============================================================
#Contour detection and crop====================================
#==============================================================
'''

'''import numpy as np
import cv2

imcolor = cv2.imread('testCursive.png', cv2.IMREAD_COLOR)
imgray = cv2.cvtColor(imcolor, cv2.COLOR_BGR2GRAY)
ret,thresh = cv2.threshold(imgray,200,255,cv2.THRESH_BINARY_INV)
image, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

print(contours)
#cv2.drawContours(imcolor, contours, -1, (0, 0, 255), 2)

for cnt in contours:
    x,y,w,h = cv2.boundingRect(cnt)
    cv2.rectangle(imcolor,(x,y),(x+w,y+h),(0,255,0),2)
    #cv2.rectangle(thresh,(x,y),(x+w,y+h),(0,255,0),2)

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.imshow('image', imcolor)
cv2.waitKey(0)
cv2.destroyAllWindows()
'''
'''

#==============================================================
#Canny Edge Detection==========================================
#==============================================================
import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('testCursive.png',0)
edges = cv2.Canny(img,255,255)

plt.subplot(121),plt.imshow(img,cmap = 'gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(edges,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])

plt.show()
'''