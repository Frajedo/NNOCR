# Mute tensorflow debugging information console
import os
import cv2
import time
from PIL import Image
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from keras.layers import Conv2D, MaxPooling2D, Convolution2D, Dropout, Dense, Flatten, LSTM
from keras.models import Sequential, save_model, model_from_yaml
from keras.utils import np_utils
from scipy.io import loadmat
from scipy.misc import imsave, imread, imresize
import pickle
import argparse
import keras
import numpy as np
import re
import base64
import pickle

def load_image(image_file_path):

    print("============LOADING IMAGE============")

    # -----Lecture de l'image en échelle de gris-----
    img = cv2.imread(image_file_path, cv2.IMREAD_GRAYSCALE)

    # -----Inversion de chaque bit de l'array-----
    img = cv2.bitwise_not(img)

    # -----Création de l'image -----
    base = np.zeros((28, 28), np.uint8)

    cv2.imshow("PRE DENOISING / DILATION", img)
    cv2.waitKey(0)

    # -----Lissage de l'image pour réduire le bruit -----
    img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    img = cv2.fastNlMeansDenoisingColored(img,None,10,10,7,21)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # ----- Application du seuil FIXE -----
    ret, img = cv2.threshold(img, 80, 255, cv2.THRESH_BINARY)

    # ----- [EXPERIMENTAL] Application du seuil ADAPTATIF [EXPERIMENTAL] -----
    #img = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
    #        cv2.THRESH_BINARY,13,10)
    #img = np.bitwise_not(img)

    kernel = np.ones((6, 6), np.uint8)

    img = cv2.dilate(img, kernel, iterations=1)

    cv2.imshow("POST DILATION", img)
    cv2.waitKey(0)

    # -----Contour detection-----
    image, contours, hierarchy = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # ----- [EXPERIMENTAL] Squeletisation [EXPERIMENTAL] -----
    # -----Obtention des éléments structurants dans une matrice 3x3-----
    '''element = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))

    
    done = False
    while (not done):

        zeros = size - cv2.countNonZero(img)
        if zeros == size:
            done = True

        eroded = cv2.erode(img, element)
        temp = cv2.dilate(eroded, element)
        temp = cv2.subtract(img, temp)
        skel = cv2.bitwise_or(skel, temp, skel)
        img = eroded.copy()

    cv2.namedWindow('image', cv2.WINDOW_NORMAL)
    cv2.imshow("FINAL", skel)
    cv2.waitKey(0)'''


    # ----- Contour sorting from left to right -----
    contours, boxes = sort_contours(contours)
    i = 0
    for cnt in contours:
        i += 1
        x, y, w, h = cv2.boundingRect(cnt)
        crop_img = img[y:y + h, x:x + w]

        # ----- Calculate ratio -----
        h2, w2 = base.shape
        w_result = w / w2

        h_result = h / h2

        # -----Resize image to meet 28x28 px expectation -----
        if (w_result > h_result):
            small = cv2.resize(crop_img, (int(w / w_result), int(h / w_result)))
        else:
            small = cv2.resize(crop_img, (int(w / h_result), int(h / h_result)))

        x, y = small.shape
        posX = int((base.shape[1] - x) / 2)
        posY = int((base.shape[0] - y) / 2)

        base[posX:posX + x, posY:posY + y] = small

        imageName = "characters/image" + str(i) + ".png"
        cv2.imwrite(imageName, base)

        base = np.zeros((28, 28), np.uint8)

def sort_contours(cnts):
    # initialize the reverse flag and sort index
    reverse = False
    i = 0

    # construct the list of bounding boxes and sort them from top to
    # bottom
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
                                        key=lambda b: b[1][i], reverse=reverse))

    # return the list of sorted contours and bounding boxes
    return (cnts, boundingBoxes)

def load_data(mat_file_path, width=28, height=28, max_=None, verbose=True):

    print("============Load data============")
    # Local functions
    def rotate(img):
        # Used to rotate images (for some reason they are transposed on read-in)
        flipped = np.fliplr(img)
        return np.rot90(flipped)

    def display(img, threshold=0.5):
        # Debugging only
        render = ''
        for row in img:
            for col in row:
                if col > threshold:
                    render += '@'
                else:
                    render += '.'
            render += '\n'
        return render

    # Load convoluted list structure form loadmat
    mat = loadmat(mat_file_path)
    print(mat['dataset'][0][0][2])

    # Load char mapping
    mapping = {kv[0]:kv[1:][0] for kv in mat['dataset'][0][0][2]}
    pickle.dump(mapping, open('bin/mapping.p', 'wb' ))

    # Load training data
    if max_ == None:
        max_ = len(mat['dataset'][0][0][0][0][0][0])
        print("TRAINING MAX: " + str(max_))
    training_images = mat['dataset'][0][0][0][0][0][0][:max_].reshape(max_, height, width, 1)
    training_labels = mat['dataset'][0][0][0][0][0][1][:max_]

    max_ = None
    # Load testing data
    if max_ == None:
        max_ = len(mat['dataset'][0][0][1][0][0][0])
        print("TESTING MAX: " + str(max_))
    else:
        max_ = int(max_ / 6)
    testing_images = mat['dataset'][0][0][1][0][0][0][:max_].reshape(max_, height, width, 1)
    testing_labels = mat['dataset'][0][0][1][0][0][1][:max_]

    # Reshape training data to be valid
    if verbose == True: _len = len(training_images)
    for i in range(len(training_images)):
        if verbose == True: print('%d/%d (%.2lf%%)' % (i + 1, _len, ((i + 1)/_len) * 100), end='\r')
        training_images[i] = rotate(training_images[i])
    if verbose == True: print('')

    # Reshape testing data to be valid
    if verbose == True: _len = len(testing_images)
    for i in range(len(testing_images)):
        if verbose == True: print('%d/%d (%.2lf%%)' % (i + 1, _len, ((i + 1)/_len) * 100), end='\r')
        testing_images[i] = rotate(testing_images[i])
    if verbose == True: print('')

    # Convert type to float32
    training_images = training_images.astype('float32')
    testing_images = testing_images.astype('float32')

    # Normalize to prevent issues with model
    training_images /= 255
    testing_images /= 255

    nb_classes = len(mapping)

    return ((training_images, training_labels), (testing_images, testing_labels), mapping, nb_classes)

def build_net(training_data, width=28, height=28, verbose=False):

    print("============Building neural network============")

    # Initialize data
    (x_train, y_train), (x_test, y_test), mapping, nb_classes = training_data
    input_shape = (height, width, 1)

    # Hyperparameters
    nb_filters = 32  # number of convolutional filters to use
    pool_size = (2, 2)  # size of pooling area for max pooling
    kernel_size = (3, 3)  # convolution kernel size

    model = Sequential()
    model.add(Convolution2D(nb_filters,
                            kernel_size,
                            padding='valid',
                            input_shape=input_shape,
                            activation='relu'))
    model.add(Convolution2D(nb_filters,
                            kernel_size,
                            activation='relu'))

    model.add(MaxPooling2D(pool_size=pool_size))
    model.add(Dropout(0.25))
    model.add(Flatten())

    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(nb_classes, activation='softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer='adadelta',
                  metrics=['accuracy'])

    model.summary()
    return model

def train(model, training_data, callback=True, batch_size=256, epochs=3):
    (x_train, y_train), (x_test, y_test), mapping, nb_classes = training_data

    # convert class vectors to binary class matrices
    y_train = np_utils.to_categorical(y_train-1, nb_classes)
    y_test = np_utils.to_categorical(y_test-1, nb_classes)

    if callback == True:
        # Callback for analysis in TensorBoard
        tbCallBack = keras.callbacks.TensorBoard(log_dir='./Graph', histogram_freq=0, write_graph=True, write_images=True)

    model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              verbose=1,
              validation_data=(x_test, y_test),
              callbacks=[tbCallBack] if callback else None)

    score = model.evaluate(x_test, y_test, verbose=0)
    print('Test score:', score[0])
    print('Test accuracy:', score[1])

    # Offload model to file
    model_yaml = model.to_yaml()
    with open("bin/model.yaml", "w") as yaml_file:
        yaml_file.write(model_yaml)
    save_model(model, 'bin/model.h5')

def load_model(bin_dir):
    # load YAML and create model
    yaml_file = open('%s/model.yaml' % bin_dir, 'r')
    loaded_model_yaml = yaml_file.read()
    yaml_file.close()
    model = model_from_yaml(loaded_model_yaml)

    # load weights into new model
    model.load_weights('%s/model.h5' % bin_dir)
    return model

def predict(imgName):

    # read parsed image back in 8-bit, black and white mode (L)
    x = imread(imgName)

    # reshape image data for use in neural network
    x = x.reshape(1,28,28,1)

    # Convert type to float32
    x = x.astype('float32')

    # Normalize to prevent issues with model
    x /= 255

    # Predict from model
    out = model.predict(x)

    print("=============")
    print(str(np.argmax(out, axis=1)))
    print("=============")

    # Generate response
    #response = {'prediction': chr(mapping[(int(np.argmax(out, axis=1)[0])+1)]),
    #            'confidence': str(max(out[0]) * 100)[:6]}

    response = chr(mapping[(int(np.argmax(out, axis=1)[0])+1)]);

    return response;

if __name__ == '__main__':

    print("===============================")
    print("===WELCOME TO VP & JBS NNOCR===")
    print("===============================")
    print("1 – MODEL TRAINING ONLY")
    print("2 – IMAGE UPLOAD")
    print("3 – CHARACTER RECOGNITION")

    path, dirs, files = next(os.walk("characters/"))
    file_count = len(files)

    text = input("=>")

    if(text == "1"):
        mat_path = input("mat_path =>")
        if(mat_path == "default"):
            training_data = load_data("datasets/emnist-letters.mat")
        else:
            training_data = load_data(mat_path)
        model = build_net(training_data)
        train(model, training_data)

    elif(text == "2"):
        img_path = input("img_path =>")
        load_image(img_path)

    elif(text == "3"):

        mapping = pickle.load(open('bin/mapping.p', 'rb'))

        model = load_model("bin");

        result=""
        for x in range(1, file_count):
            result += predict("characters/image" + str(x) + ".png")
            print(result)